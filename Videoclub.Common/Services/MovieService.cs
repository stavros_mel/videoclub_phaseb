﻿using System.Collections.Generic;
using System.Linq;
using Videoclub.Core.Entities;
using Videoclub.Core.Interfaces;
using Videoclub.Infrastructure.Data;

namespace Videoclub.Common.Services
{
    public class MovieService : IMovieService
    {
        private readonly VideoclubDbContext _context;

        public MovieService(VideoclubDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Movie> GetAllMovies()
        {
            return _context.Movies.ToList();
        }

        public Movie GetMovie(int movieId)
        {
            return _context.Movies.FirstOrDefault(m => m.MovieId == movieId);
        }

        public Movie GetMovieByTitle(string title)
        {
            return _context.Movies.FirstOrDefault(m => m.Title == title);
        }

        public void AddMovieInDb(Movie movie)
        {
            _context.Movies.Add(movie);
            _context.SaveChanges();
        }

        public void UpdateMovie(Movie newMovie)
        {
            var existingMovie = _context.Movies.Find(newMovie.MovieId);

            if (existingMovie != null)
            {
                existingMovie.Title = newMovie.Title;
                existingMovie.Description = newMovie.Description;
                existingMovie.Genre = newMovie.Genre;
                existingMovie.NumberOfCopies = newMovie.NumberOfCopies;

                _context.SaveChanges();
            }
        }

        public void DeleteMovie(int movieId)
        {
            var movie = GetMovie(movieId);

            if (movie != null)
            {
                _context.Movies.Remove(movie);
                _context.SaveChanges();
            }
            
        }

        public bool MovieAlreadyExists(string title)
        {
            var movie = _context.Movies.FirstOrDefault(m => m.Title == title);

            return movie != null;
        }
    }
}
