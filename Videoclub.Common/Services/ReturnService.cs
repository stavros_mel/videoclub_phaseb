﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Videoclub.Core.Entities;
using Videoclub.Core.Interfaces;
using Videoclub.Infrastructure.Data;

namespace Videoclub.Common.Services
{
    public class ReturnService : IReturnService
    {
        private readonly VideoclubDbContext _context;

        public ReturnService(VideoclubDbContext context)
        {
            _context = context;
        }

        public void AddReturn(Return rentalReturn)
        {
            _context.Returns.Add(rentalReturn);
            _context.SaveChanges();
        }

        public IEnumerable<Return> GetAllReturns()
        {
            return _context.Returns.ToList();
        }
    }
}
