﻿using System.Linq;
using Videoclub.Core.Entities;
using Videoclub.Core.Interfaces;
using Videoclub.Infrastructure.Data;

namespace Videoclub.Common.Services
{
    public class CopyService : ICopyService
    {
        private readonly VideoclubDbContext _context;

        public CopyService(VideoclubDbContext context)
        {
            _context = context;
        }

        public void AddCopiesOfMovie(int numberOfCopies, int movieId)
        {
            for (var i = 0; i < numberOfCopies; i++)
            {
                _context.Copies.Add(new Copy(movieId));
            }
            _context.SaveChanges();
        }

        public Copy GetCopy(int copyId)
        {
            return _context.Copies.FirstOrDefault(c => c.CopyId == copyId);
        }

        public Copy GetAvailableCopy(int movieId)
        {
            return _context.Copies.Where(c => c.IsAvailable).FirstOrDefault(c => c.MovieId == movieId);
        }

        public void SetCopyAvailable(int copyId)
        {
            var copy = _context.Copies.Find(copyId);

            if (copy != null)
            {
                copy.IsAvailable = true;
                _context.SaveChanges();
            }
        }

        public void SetCopyUnavailable(int copyId)
        {
            var copy = _context.Copies.Find(copyId);

            if (copy != null)
            {
                copy.IsAvailable = false;
                _context.SaveChanges();
            }
        }
    }
}
