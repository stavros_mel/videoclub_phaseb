﻿using System.Collections.Generic;
using System.Linq;
using Videoclub.Core.Entities;
using Videoclub.Core.Interfaces;
using Videoclub.Infrastructure.Data;

namespace Videoclub.Common.Services
{
    public class RentalService : IRentalService
    {
        private readonly VideoclubDbContext _context;

        public RentalService(VideoclubDbContext context)
        {
            _context = context;
        }

        public void AddRental(Rental rental)
        {
            _context.Rentals.Add(rental);
            _context.SaveChanges();
        }

        public Rental GetRental(int rentalId)
        {
            return _context.Rentals.FirstOrDefault(r => r.Id == rentalId);
        }

        public IEnumerable<Rental> GetAllRentals()
        {
            return _context.Rentals.ToList();
        }

        public IEnumerable<Rental> GetAllActiveRentals()
        {
            return _context.Rentals.Where(r => r.IsActive).ToList();
        }

        public void SetRentalActive(Rental rental)
        {
            rental.IsActive = true;
            _context.SaveChanges();
        }

        public void SetRentalInactive(Rental rental)
        {
            rental.IsActive = false;
            _context.SaveChanges();
        }
    }
}
