﻿using System.Collections.Generic;
using System.Linq;
using Videoclub.Core.Entities;
using Videoclub.Core.Interfaces;
using Videoclub.Infrastructure.Data;

namespace Videoclub.Common.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly VideoclubDbContext _context;

        public CustomerService(VideoclubDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Customer> GetAllCustomers()
        {
            return _context.Customers.ToList();

        }

        public Customer GetCustomer(string customerId)
        {
            return _context.Customers.FirstOrDefault(u => u.ApplicationUserId == customerId);
        }

        public Customer GetCustomerByEmail(string customerEmail)
        {
            return _context.Customers.FirstOrDefault(c => c.Email == customerEmail);
        }

        public void IncreaseActiveRentals(string customerId)
        {
            var customer = GetCustomer(customerId);
            customer.NumberOfActiveRentals++;
            _context.SaveChanges();
        }

        public void DecreaseActiveRentals(string customerId)
        {
            var customer = GetCustomer(customerId);
            customer.NumberOfActiveRentals--;
            _context.SaveChanges();
        }

        public IEnumerable<Rental> GetRentalsOfCustomer(string customerId)
        {
            return _context.Rentals.Where(r => r.CustomerId == customerId);
        }

        public void DeleteCustomer(string customerId)
        {
            var customer = GetCustomer(customerId);

            if (customer != null)
            {
                _context.Customers.Remove(customer);
                _context.SaveChanges();
            }
        }

        public void UpdateCustomer(Customer newCustomer)
        {
            var existingCustomer = GetCustomer(newCustomer.ApplicationUserId);

            if (existingCustomer != null)
            {
                existingCustomer.FirstName = newCustomer.FirstName;
                existingCustomer.LastName = newCustomer.LastName;
                existingCustomer.Email = newCustomer.Email;

                _context.SaveChanges();
            }
        }

        public void AddCustomer(Customer customer)
        {
            _context.Customers.Add(customer);
            _context.SaveChanges();
        }
    }
}
