﻿namespace Videoclub.Core.Enumerations
{
    public enum MovieGenre
    {
        Action = 1,
        Drama = 2,
        Thriller = 3,
        Horror = 4,
        Comedy = 5,
        Romantic = 6,
        Adventure = 7,
        Cartoon = 8
    }
}
