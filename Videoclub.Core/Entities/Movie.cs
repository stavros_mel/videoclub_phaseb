﻿using System.Collections.Generic;
using Videoclub.Core.Enumerations;

namespace Videoclub.Core.Entities
{
    public class Movie
    {
        public int MovieId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public MovieGenre Genre { get; set; }
        public int NumberOfCopies { get; set; }
        public virtual ICollection<Copy> Copies { get; set; }


        public Movie(string title, string description, MovieGenre genre, int numberOfCopies)
        {
            this.MovieId = MovieId;
            this.Title = title;
            this.Description = description;
            this.Genre = genre;
            this.NumberOfCopies = numberOfCopies;
        }

        public Movie(int id, string title, string description, MovieGenre genre, int numberOfCopies)
        {
            this.MovieId = id;
            this.Title = title;
            this.Description = description;
            this.Genre = genre;
            this.NumberOfCopies = numberOfCopies;
        }

        public Movie()
        {
            
        }
    }
}
