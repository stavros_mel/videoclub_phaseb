﻿using System.ComponentModel.DataAnnotations;

namespace Videoclub.Core.Entities
{
    public class Copy
    {
        [Key]
        public int CopyId { get; set; }
        public virtual Movie Movie { get; set; }
        public int MovieId { get; set; }
        public bool IsAvailable { get; set; }

        public Copy(int movieId)
        {
            CopyId = CopyId;
            MovieId = movieId;
            IsAvailable = true;
        }

        public Copy()
        {
            
        }
    }
}
