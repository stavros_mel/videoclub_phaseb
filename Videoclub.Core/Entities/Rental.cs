﻿using System;

namespace Videoclub.Core.Entities
{
    public class Rental
    {
        public int Id { get; set; }
        public DateTime DateRented { get; set; }
        public DateTime ReturnDate { get; set; }
        public bool IsActive { get; set; }
        public string Comment { get; set; }
        public int CopyId { get; set; }
        public string CustomerId { get; set; }
        
        public virtual Copy Copy { get; set; }
        public virtual Customer Customer { get; set; }

        public Rental(int copyId, string comment, string customerId)
        {
            Id = Id;
            CopyId = copyId;
            Comment = comment;
            CustomerId = customerId;
            DateRented = DateTime.Now;
            ReturnDate = DateRented.AddDays(7);
            IsActive = true;
        }

        public Rental()
        {
            
        }
    }
}
