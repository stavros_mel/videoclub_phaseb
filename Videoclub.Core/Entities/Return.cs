﻿using System;

namespace Videoclub.Core.Entities
{
    public class Return
    {
        public int Id { get; set; }
        public DateTime ExpectedReturnDate { get; set; }
        public DateTime ActualReturnDate { get; set; }
        public string Comment { get; set; }
        public int CopyId { get; set; }
        public string CustomerId { get; set; }

        public virtual Copy Copy { get; set; }
        public virtual Customer Customer { get; set; }

        public Return()
        {
            
        }

        public Return(DateTime expectedReturnDate, string comment, string customerId, int copyId)
        {
            Id = Id;
            ExpectedReturnDate = expectedReturnDate;
            ActualReturnDate = DateTime.Now;
            Comment = comment;
            CustomerId = customerId;
            CopyId = copyId;
        }
    }
}
