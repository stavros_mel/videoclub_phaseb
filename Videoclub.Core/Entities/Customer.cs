﻿using System.Collections.Generic;
using Videoclub.Core.Enumerations;

namespace Videoclub.Core.Entities
{
    public class Customer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int NumberOfActiveRentals { get; set; }
        public MovieGenre Favourites { get; set; }
        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual ICollection<Rental> Rentals { get; set; }
        public virtual ICollection<Return> Returns { get; set; }

        public Customer(string firstName, string lastName, string email, MovieGenre favourites, string applicationUserId)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Favourites = favourites;
            ApplicationUserId = applicationUserId;
            NumberOfActiveRentals = 0;
        }

        public Customer()
        {
            
        }
    }
}
