﻿using Videoclub.Core.Entities;

namespace Videoclub.Core.Interfaces
{
    public interface ICopyService
    {
        void AddCopiesOfMovie(int numberOfCopies, int movieId);
        Copy GetCopy(int copyId);
        Copy GetAvailableCopy(int movieId);
        void SetCopyAvailable(int copyId);
        void SetCopyUnavailable(int copyId);
    }
}
