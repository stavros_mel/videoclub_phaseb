﻿using System.Collections.Generic;
using Videoclub.Core.Entities;

namespace Videoclub.Core.Interfaces
{
    public interface IMovieService
    {
        IEnumerable<Movie> GetAllMovies();
        Movie GetMovie(int movieId);
        Movie GetMovieByTitle(string title);
        void AddMovieInDb(Movie movie);
        void UpdateMovie(Movie movie);
        void DeleteMovie(int movieId);
        bool MovieAlreadyExists(string title);
    }
}
