﻿using System.Collections.Generic;
using Videoclub.Core.Entities;

namespace Videoclub.Core.Interfaces
{
    public interface IRentalService
    {
        void AddRental(Rental rental);
        Rental GetRental(int rentalId);
        IEnumerable<Rental> GetAllRentals();
        IEnumerable<Rental> GetAllActiveRentals();
        void SetRentalActive(Rental rental);
        void SetRentalInactive(Rental rental);

    }
}
