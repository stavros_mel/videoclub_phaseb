﻿using System.Collections;
using System.Collections.Generic;
using Videoclub.Core.Entities;

namespace Videoclub.Core.Interfaces
{
    public interface IReturnService
    {
        void AddReturn(Return rentalReturn);
        IEnumerable<Return> GetAllReturns();
    }
}
