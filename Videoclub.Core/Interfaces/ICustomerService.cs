﻿using System.Collections.Generic;
using Videoclub.Core.Entities;

namespace Videoclub.Core.Interfaces
{
    public interface ICustomerService
    {
        IEnumerable<Customer> GetAllCustomers();
        Customer GetCustomer(string customerId);
        Customer GetCustomerByEmail(string customerEmail);
        void IncreaseActiveRentals(string customerId);
        void DecreaseActiveRentals(string customerId);
        IEnumerable<Rental> GetRentalsOfCustomer(string customerId);
        void DeleteCustomer(string customerId);
        void UpdateCustomer(Customer customer);
        void AddCustomer(Customer customer);
    }
}
