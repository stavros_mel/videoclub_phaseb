﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using Videoclub.Core.Entities;
using Videoclub.Infrastructure.Data;

[assembly: OwinStartup(typeof(Videoclub.Web.Startup))]
namespace Videoclub.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
