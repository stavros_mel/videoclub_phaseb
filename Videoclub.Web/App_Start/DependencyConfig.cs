﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Videoclub.Common.Services;
using Videoclub.Core.Interfaces;
using Videoclub.Infrastructure.Data;
using Videoclub.Infrastructure.Services.Implementations;
using Videoclub.Infrastructure.Services.Interfaces;

namespace Videoclub.Web
{
    public class DependencyConfig
    {
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            // Controllers
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // Services
            builder.RegisterType<MovieService>().As<IMovieService>().InstancePerRequest();
            builder.RegisterType<CopyService>().As<ICopyService>().InstancePerRequest();
            builder.RegisterType<CustomerService>().As<ICustomerService>().InstancePerRequest();
            builder.RegisterType<RentalService>().As<IRentalService>().InstancePerRequest();
            builder.RegisterType<ReturnService>().As<IReturnService>().InstancePerRequest();
            builder.RegisterType<LoggingService>().As<ILoggingService>().InstancePerRequest();

            // Db Context
            builder.RegisterType<VideoclubDbContext>().As<VideoclubDbContext>().InstancePerRequest();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}