﻿using System.IO;
using System.Linq;
using System.Web.Mvc;
using OfficeOpenXml;
using Videoclub.Core.Entities;
using Videoclub.Core.Interfaces;
using Videoclub.Web.Areas.Rentals.Models;

namespace Videoclub.Web.Areas.Rentals.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RentalsController : Controller
    {
        private readonly IRentalService _rentalsDb;
        private readonly IReturnService _returnsDb;
        private readonly ICopyService _copiesDb;
        private readonly ICustomerService _customersDb;

        public RentalsController()
        {
            
        }

        public RentalsController(IRentalService rentalsDb, IReturnService returnsDb, ICopyService copiesDb, ICustomerService customersDb)
        {
            _rentalsDb = rentalsDb;
            _returnsDb = returnsDb;
            _copiesDb = copiesDb;
            _customersDb = customersDb;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var activeRentals = _rentalsDb.GetAllActiveRentals();
            var model = activeRentals.Select(rental => new RentalViewModel(rental)).ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult ExportActiveRentals()
        {
            var rentals = _rentalsDb.GetAllActiveRentals();
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("all_rentals");
                worksheet.Cells[1, 1].Value = "ID";
                worksheet.Cells[1, 2].Value = "CUSTOMER";
                worksheet.Cells[1, 3].Value = "DATE_RENTED";
                worksheet.Cells[1, 4].Value = "DATE_RETURNED";
                worksheet.Cells[1, 5].Value = "IS_ACTIVE";
                worksheet.Cells[1, 6].Value = "TITLE";
                worksheet.Cells[1, 7].Value = "COPY_ID";
                worksheet.Cells[1, 8].Value = "COMMENT";

                var rowIndex = 2;

                foreach (var rental in rentals)
                {
                    worksheet.Cells[rowIndex, 1].Value = rental.Id;
                    worksheet.Cells[rowIndex, 2].Value = rental.Customer.Email;
                    worksheet.Cells[rowIndex, 3].Value = $"{rental.DateRented:dd-MM-yyyy HH:mm}";
                    worksheet.Cells[rowIndex, 4].Value = $"{rental.ReturnDate:dd-MM-yyyy HH:mm}";
                    worksheet.Cells[rowIndex, 5].Value = rental.IsActive;
                    worksheet.Cells[rowIndex, 6].Value = rental.Copy.Movie.Title;
                    worksheet.Cells[rowIndex, 7].Value = rental.CopyId;
                    worksheet.Cells[rowIndex, 8].Value = rental.Comment;

                    rowIndex++;
                }

                worksheet.Cells.AutoFitColumns();


                var stream = new MemoryStream();
                package.SaveAs(stream);
                stream.Seek(0, SeekOrigin.Begin);

                return File(
                    stream,
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    "Videoclub_rentals.xlsx"
                );
            }
        }

        public ActionResult ExportReturns()
        {
            var returns = _returnsDb.GetAllReturns();
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("all_returns");
                worksheet.Cells[1, 1].Value = "ID";
                worksheet.Cells[1, 2].Value = "CUSTOMER";
                worksheet.Cells[1, 3].Value = "EXPECTED_RETURN_DATE";
                worksheet.Cells[1, 4].Value = "ACTUAL_RETURN_DATE";
                worksheet.Cells[1, 5].Value = "TITLE";
                worksheet.Cells[1, 6].Value = "COPY_ID";
                worksheet.Cells[1, 7].Value = "COMMENT";

                var rowIndex = 2;

                foreach (var rentalReturn in returns)
                {
                    worksheet.Cells[rowIndex, 1].Value = rentalReturn.Id;
                    worksheet.Cells[rowIndex, 2].Value = rentalReturn.Customer.Email;
                    worksheet.Cells[rowIndex, 3].Value = $"{rentalReturn.ActualReturnDate:dd-MM-yyyy HH:mm}";
                    worksheet.Cells[rowIndex, 4].Value = $"{rentalReturn.ExpectedReturnDate:dd-MM-yyyy HH:mm}";
                    worksheet.Cells[rowIndex, 5].Value = rentalReturn.Copy.Movie.Title;
                    worksheet.Cells[rowIndex, 6].Value = rentalReturn.CopyId;
                    worksheet.Cells[rowIndex, 7].Value = rentalReturn.Comment;

                    rowIndex++;
                }

                worksheet.Cells.AutoFitColumns();


                var stream = new MemoryStream();
                package.SaveAs(stream);
                stream.Seek(0, SeekOrigin.Begin);

                return File(
                    stream,
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    "Videoclub_returns.xlsx"
                );
            }
        }

        [HttpGet]
        public ActionResult ReturnRental(int id)
        {
            var rental = _rentalsDb.GetRental(id);
            var model = new ReturnBindingModel(rental.Copy.Movie.Title, rental.Customer.Email, rental.Id);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ReturnRental(ReturnBindingModel model)
        {
            var rental = _rentalsDb.GetRental(model.RentalId);
            var rentalReturn = new Return(rental.ReturnDate, model.Comment, rental.CustomerId, rental.CopyId);

            _returnsDb.AddReturn(rentalReturn);
            _rentalsDb.SetRentalInactive(rental);
            _copiesDb.SetCopyAvailable(rental.CopyId);
            _customersDb.DecreaseActiveRentals(rental.CustomerId);

            return RedirectToAction("Index");
        }
    }
}