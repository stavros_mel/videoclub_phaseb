﻿using System.ComponentModel.DataAnnotations;

namespace Videoclub.Web.Areas.Rentals.Models
{
    public class ReturnBindingModel
    {
        [Required]
        [Display(Name = "Movie Title")]
        public string MovieTitle { get; set; }

        [Required]
        [Display(Name = "Customer Email")]
        public string CustomerEmail { get; set; }

        public string Comment { get; set; }

        public int RentalId { get; set; }

        public ReturnBindingModel(string movieTitle, string customerEmail, int rentalId)
        {
            MovieTitle = movieTitle;
            CustomerEmail = customerEmail;
            RentalId = rentalId;
        }

        public ReturnBindingModel()
        {
            
        }
    }
}