﻿using System;
using System.ComponentModel.DataAnnotations;
using Videoclub.Core.Entities;

namespace Videoclub.Web.Areas.Rentals.Models
{
    public class RentalViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Movie Title")]
        public string MovieTitle { get; set; }

        [Display(Name = "Copy ID")]
        public int CopyId { get; set; }

        [Display(Name = "Date Rented")]
        public DateTime DateRented { get; set; }

        [Display(Name = "Return Date")]
        public DateTime ReturnDate { get; set; }

        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }

        public string Comment { get; set; }

        [Display(Name = "Customer Email")]
        public string CustomerEmail { get; set; }

        public RentalViewModel()
        {
            
        }

        public RentalViewModel(Rental rental)
        {
            Id = rental.Id;
            MovieTitle = rental.Copy.Movie.Title;
            CopyId = rental.CopyId;
            DateRented = rental.DateRented;
            ReturnDate = rental.ReturnDate;
            IsActive = rental.IsActive;
            Comment = rental.Comment;
            CustomerEmail = rental.Customer.Email;
        }
    }
}