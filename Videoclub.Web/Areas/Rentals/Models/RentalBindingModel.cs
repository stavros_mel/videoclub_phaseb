﻿using System.ComponentModel.DataAnnotations;

namespace Videoclub.Web.Areas.Rentals.Models
{
    public class RentalBindingModel
    {
        [Required]
        [Display(Name = "Movie Title")]
        public string MovieTitle { get; set; }

        [Required]
        [Display(Name = "Customer Email")]
        public string CustomerEmail { get; set; }

        public string Comment { get; set; }

        public RentalBindingModel(string movieTitle)
        {
            MovieTitle = movieTitle;
        }

        public RentalBindingModel()
        {
            
        }
    }
}