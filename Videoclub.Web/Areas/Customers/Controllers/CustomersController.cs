﻿using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using OfficeOpenXml;
using Videoclub.Core.Interfaces;
using Videoclub.Web.Areas.Customers.Models;
using Videoclub.Web.Areas.Rentals.Models;

namespace Videoclub.Web.Areas.Customers.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CustomersController : Controller
    {
        private readonly ICustomerService _customersDb;

        public CustomersController()
        {
            
        }

        public CustomersController(ICustomerService customersDb)
        {
            _customersDb = customersDb;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var customers = _customersDb.GetAllCustomers();
            var model = customers.Select(customer => new CustomerViewModel(customer)).ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult ExportCustomers()
        {
            var customers = _customersDb.GetAllCustomers();
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("customers");
                worksheet.Cells[1, 1].Value = "ID";
                worksheet.Cells[1, 2].Value = "F_NAME";
                worksheet.Cells[1, 3].Value = "L_NAME";
                worksheet.Cells[1, 4].Value = "EMAIL";
                worksheet.Cells[1, 5].Value = "ACTIVE_RENTALS";
                worksheet.Cells[1, 6].Value = "FAVOURITES";

                var rowIndex = 2;

                foreach (var customer in customers)
                {
                    worksheet.Cells[rowIndex, 1].Value = customer.ApplicationUserId;
                    worksheet.Cells[rowIndex, 2].Value = customer.FirstName;
                    worksheet.Cells[rowIndex, 3].Value = customer.LastName;
                    worksheet.Cells[rowIndex, 4].Value = customer.Email;
                    worksheet.Cells[rowIndex, 5].Value = customer.NumberOfActiveRentals;
                    worksheet.Cells[rowIndex, 6].Value = customer.Favourites;

                    rowIndex++;
                }

                worksheet.Cells.AutoFitColumns();


                var stream = new MemoryStream();
                package.SaveAs(stream);
                stream.Seek(0, SeekOrigin.Begin);

                return File(
                    stream,
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    "Videoclub_customers.xlsx"
                );
            }
        }

        [HttpGet]
        public ActionResult DeleteCustomer(string id)
        {
            var customer = _customersDb.GetCustomer(id);
            var model = new CustomerViewModel(customer);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteCustomer(string id, FormCollection form)
        {
            var customer = _customersDb.GetCustomer(id);

            if (customer != null)
            {
                _customersDb.DeleteCustomer(customer.ApplicationUserId);

                return RedirectToAction("Index");
            }

            return View();
        }

        [HttpGet]
        public ActionResult CustomerRentals(string id)
        {
            var customerRentals = _customersDb.GetRentalsOfCustomer(id);
            var model = customerRentals.Select(rental => new RentalViewModel(rental)).ToList();

            ViewBag.TotalRentals = model.Count();
            ViewBag.CustomerEmail = _customersDb.GetCustomer(id).Email;
            ViewBag.CustomerId = id;

            return View(model);
        }

        [HttpGet]
        public ActionResult ExportCustomerRentals(string id)
        {
            var customerRentals = _customersDb.GetRentalsOfCustomer(id);
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("customer_rentals");
                worksheet.Cells[1, 1].Value = "ID";
                worksheet.Cells[1, 2].Value = "DATE_RENTED";
                worksheet.Cells[1, 3].Value = "DATE_RETURNED";
                worksheet.Cells[1, 4].Value = "IS_ACTIVE";
                worksheet.Cells[1, 5].Value = "TITLE";
                worksheet.Cells[1, 6].Value = "COPY_ID";
                worksheet.Cells[1, 7].Value = "COMMENT";

                var rowIndex = 2;

                foreach (var rental in customerRentals)
                {
                    worksheet.Cells[rowIndex, 1].Value = rental.Id;
                    worksheet.Cells[rowIndex, 2].Value = $"{rental.DateRented:dd-MM-yyyy HH:mm}";
                    worksheet.Cells[rowIndex, 3].Value = $"{rental.ReturnDate:dd-MM-yyyy HH:mm}";
                    worksheet.Cells[rowIndex, 4].Value = rental.IsActive;
                    worksheet.Cells[rowIndex, 5].Value = rental.Copy.Movie.Title;
                    worksheet.Cells[rowIndex, 6].Value = rental.CopyId;
                    worksheet.Cells[rowIndex, 7].Value = rental.Comment;

                    rowIndex++;
                }

                worksheet.Cells.AutoFitColumns();


                var stream = new MemoryStream();
                package.SaveAs(stream);
                stream.Seek(0, SeekOrigin.Begin);

                return File(
                    stream,
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    $"{customerRentals.First().Customer.Email}_rentals.xlsx"
                );
            }
        }
    }
}