﻿using System.ComponentModel.DataAnnotations;
using Videoclub.Core.Entities;

namespace Videoclub.Web.Areas.Customers.Models
{
    public class CustomerViewModel
    {
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        public string  Email { get; set; }

        [Display(Name = "Active Rentals")]
        public int  NumberOfActiveRentals { get; set; }

        public string ApplicationUserId { get; set; }

        public CustomerViewModel(Customer customer)
        {
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Email = customer.Email;
            NumberOfActiveRentals = customer.NumberOfActiveRentals;
            ApplicationUserId = customer.ApplicationUserId;
        }
    }
}