﻿namespace Videoclub.Web.Areas.Customers.Models
{
    public class CustomerBindingModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int ActiveRentals { get; set; }
        public string ApplicationUserId { get; set; }

        public CustomerBindingModel(string firstName, string lastName, string email, int activeRentals, string applicationUserId)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            ActiveRentals = activeRentals;
            ApplicationUserId = applicationUserId;
        }

        public CustomerBindingModel()
        {
            
        }
    }
}