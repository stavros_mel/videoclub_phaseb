﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using PagedList;
using Videoclub.Core.Entities;
using Videoclub.Core.Interfaces;
using Videoclub.Infrastructure.Services.Interfaces;
using Videoclub.Web.Areas.Movies.Models;
using Videoclub.Web.Areas.Rentals.Models;
using OfficeOpenXml;

namespace Videoclub.Web.Areas.Movies.Controllers
{
    public class MoviesController : Controller
    {
        private readonly IMovieService _moviesDb;
        private readonly ICopyService _copiesDb;
        private readonly IRentalService _rentalsDb;
        private readonly ICustomerService _customersDb;
        private readonly ILoggingService _loggingService;

        public MoviesController()
        {
            
        }

        public MoviesController(IMovieService moviesDb, ICopyService copiesDb, IRentalService rentalsDb, ICustomerService customersDb, ILoggingService loggingService)
        {
            _moviesDb = moviesDb;
            _copiesDb = copiesDb;
            _rentalsDb = rentalsDb;
            _customersDb = customersDb;
            _loggingService = loggingService;
        }

        [HttpGet]
        [Authorize(Roles = "Admin, Customer")]
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var movies = _moviesDb.GetAllMovies();

            if (!String.IsNullOrEmpty(searchString))
            {
                movies = movies.Where(m => m.Title.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "title_desc":
                    movies = movies.OrderByDescending(m => m.Title);
                    break;
                case "Genre":
                    movies = movies.OrderBy(m => m.Genre);
                    break;
                case "genre_desc":
                    movies = movies.OrderByDescending(m => m.Genre);
                    break;
                default:
                    movies = movies.OrderBy(m => m.Title);
                    break;
            }

            int moviesPerPage = 3;
            int pageNumber = (page ?? 1);

            ViewBag.MoviesCount = movies.Count();

            var model = movies.Select(movie => new MovieViewModel(movie)).ToList();

            return View(model.ToPagedList(pageNumber, moviesPerPage));
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult ExportMovies()
        {
            var movies = _moviesDb.GetAllMovies();

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("movies");
                worksheet.Cells[1, 1].Value = "ID";
                worksheet.Cells[1, 2].Value = "TITLE";
                worksheet.Cells[1, 3].Value = "DESCRIPTION";
                worksheet.Cells[1, 4].Value = "GENRE";
                worksheet.Cells[1, 5].Value = "COPIES";

                var rowIndex = 2;
                foreach (var movie in movies)
                {
                    worksheet.Cells[rowIndex, 1].Value = movie.MovieId;
                    worksheet.Cells[rowIndex, 2].Value = movie.Title;
                    worksheet.Cells[rowIndex, 3].Value = movie.Description;
                    worksheet.Cells[rowIndex, 4].Value = movie.Genre;
                    worksheet.Cells[rowIndex, 5].Value = movie.Copies.Count;

                    rowIndex++;
                }

                worksheet.Cells.AutoFitColumns();


                var stream = new MemoryStream();
                package.SaveAs(stream);
                stream.Seek(0, SeekOrigin.Begin);

                return File(
                    stream,
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    "Videoclub_Movies_Export.xlsx"
                );
            }
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult AddMovie()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult AddMovie(MovieBindingModel model)
        {
            var movie = new Movie(model.Title, model.Description, model.Genre, model.NumberOfCopies);

            if (_moviesDb.MovieAlreadyExists(movie.Title))
            {
                _loggingService.Info("Movie {Title} already exists! Can't add.", movie.Title);
                return View("~/Areas/Movies/Views/Movies/MovieAlreadyExists.cshtml");
            }

            _moviesDb.AddMovieInDb(movie);
            _copiesDb.AddCopiesOfMovie(movie.NumberOfCopies, movie.MovieId);

            _loggingService.Info("Movie {Title} added in DB!", movie.Title);

            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult UpdateMovie(int id)
        {
            var movie = _moviesDb.GetMovie(id);
            var model = new MovieBindingModel(movie.MovieId, movie.Title, movie.Description, movie.Genre, movie.NumberOfCopies);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult UpdateMovie(MovieBindingModel model)
        {
            var movie = new Movie(model.MovieId, model.Title, model.Description, model.Genre, model.NumberOfCopies);
            _moviesDb.UpdateMovie(movie);

            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteMovie(int id)
        {
            var movie = _moviesDb.GetMovie(id);
            var model = new MovieViewModel(movie);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteMovie(int id, FormCollection form)
        {
            _moviesDb.DeleteMovie(id);

            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult RentMovie(int id)
        {
            var movie = _moviesDb.GetMovie(id);
            var model = new RentalBindingModel(movie.Title);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult RentMovie(RentalBindingModel model)
        {
            var customer = _customersDb.GetCustomerByEmail(model.CustomerEmail);

            if (customer != null)
            {
                var movie = _moviesDb.GetMovieByTitle(model.MovieTitle);
                var copy = _copiesDb.GetAvailableCopy(movie.MovieId);
                var rental = new Rental(copy.CopyId, model.Comment, customer.ApplicationUserId);

                _rentalsDb.AddRental(rental);
                _copiesDb.SetCopyUnavailable(copy.CopyId);
                _customersDb.IncreaseActiveRentals(customer.ApplicationUserId);

                return RedirectToAction("Index");
            }

            ViewBag.Email = model.CustomerEmail;
            return View("~/Areas/Movies/Views/Movies/CustomerDoesntExist.cshtml");
        }
    }
}