﻿using System.Collections.Generic;
using Videoclub.Core.Entities;
using Videoclub.Core.Enumerations;

namespace Videoclub.Web.Areas.Movies.Models
{
    public class MovieViewModel
    {
        public int MovieId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public MovieGenre Genre { get; set; }
        public int NumberOfCopies { get; set; }
        public ICollection<Copy> Copies { get; set; }

        public MovieViewModel(Movie movie)
        {
            MovieId = movie.MovieId;
            Title = movie.Title;
            Description = movie.Description;
            Genre = movie.Genre;
            NumberOfCopies = movie.NumberOfCopies;
            Copies = movie.Copies;
        }
    }
}