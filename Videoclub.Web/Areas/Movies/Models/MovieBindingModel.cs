﻿using System.ComponentModel.DataAnnotations;
using Videoclub.Core.Enumerations;

namespace Videoclub.Web.Areas.Movies.Models
{
    public class MovieBindingModel
    {
        public int  MovieId { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public MovieGenre Genre { get; set; }

        [Required]
        [Display(Name = "No. of Copies")]
        public int NumberOfCopies { get; set; }

        public MovieBindingModel(string title, string description, MovieGenre genre, int numberOfCopies)
        {
            Title = title;
            Description = description;
            Genre = genre;
            NumberOfCopies = numberOfCopies;
        }

        public MovieBindingModel(int id, string title, string description, MovieGenre genre, int numberOfCopies)
        {
            MovieId = id;
            Title = title;
            Description = description;
            Genre = genre;
            NumberOfCopies = numberOfCopies;
        }

        public MovieBindingModel()
        {
            
        }
    }
}