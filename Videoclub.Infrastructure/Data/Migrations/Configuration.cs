﻿using System.Data.Entity.Migrations;

namespace Videoclub.Infrastructure.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<VideoclubDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Data\Migrations";
        }
    }
}
