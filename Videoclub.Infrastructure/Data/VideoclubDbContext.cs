﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Videoclub.Core.Entities;


namespace Videoclub.Infrastructure.Data
{
    // Enable-Migrations -ContextTypeName  Videoclub.Infrastructure.Data.VideoclubDbContext -MigrationsDirectory Data\Migrations
    // Add-Migration -ConfigurationTypeName  Videoclub.Infrastructure.Data.Migrations.Configuration Initial
    // Update-Database -ConfigurationTypeName  Videoclub.Infrastructure.Data.Migrations.Configuration
    // Update-Database -ConfigurationTypeName  Videoclub.Infrastructure.Data.Migrations.Configuration -TargetMigration:0

    public class VideoclubDbContext : IdentityDbContext<ApplicationUser>
    {
        public VideoclubDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static VideoclubDbContext Create()
        {
            return new VideoclubDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region Movie
            modelBuilder.Entity<Movie>()
                .HasMany(m => m.Copies)
                .WithRequired(c => c.Movie)
                .HasForeignKey(c => c.MovieId)
                .WillCascadeOnDelete(true);
            #endregion

            #region Customer
            modelBuilder.Entity<Customer>()
                .HasKey(c => c.ApplicationUserId);

            modelBuilder.Entity<Customer>()
                .HasMany(c => c.Rentals)
                .WithRequired(r => r.Customer)
                .HasForeignKey(r => r.CustomerId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Customer>()
                .HasMany(c => c.Returns)
                .WithRequired(r => r.Customer)
                .HasForeignKey(r => r.CustomerId)
                .WillCascadeOnDelete(true);
            #endregion

            #region ApplicationUser
            modelBuilder.Entity<ApplicationUser>()
                .HasOptional(u => u.Customer)
                .WithRequired(c => c.ApplicationUser)
                .WillCascadeOnDelete(true);
            #endregion
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Copy> Copies { get; set; }
        public DbSet<Rental> Rentals { get; set; }
        public DbSet<Return> Returns { get; set; }
        public DbSet<Customer> Customers { get; set; }
    }
}
