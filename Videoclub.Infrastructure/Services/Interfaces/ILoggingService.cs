﻿namespace Videoclub.Infrastructure.Services.Interfaces
{
    public interface ILoggingService
    {
        void Warning(string message);
        void Warning(string message, params object[] args);

        void Info(string message);
        void Info(string message, params object[] args);
    }
}