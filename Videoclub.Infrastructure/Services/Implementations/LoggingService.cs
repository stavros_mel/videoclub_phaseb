﻿using Serilog;
using Serilog.Core;
using Videoclub.Infrastructure.Services.Interfaces;

namespace Videoclub.Infrastructure.Services.Implementations
{
    public class LoggingService : ILoggingService
    {
        private readonly Logger _logger;

        public LoggingService()
        {
            _logger = new LoggerConfiguration()
                .WriteTo.File(
                    @"C:\Logs\logs.txt", 
                    rollingInterval: RollingInterval.Day)
                .CreateLogger();
        }

        public void Warning(string message)
        {
            _logger.Warning(message);
        }

        public void Warning(string message, params object[] args)
        {
            _logger.Warning(message, args);
        }

        public void Info(string message)
        {
            _logger.Information(message);
        }

        public void Info(string message, params object[] args)
        {
            _logger.Information(message, args);
        }
    }
}
