﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using Videoclub.Core.Entities;
using Videoclub.Core.Enumerations;
using Videoclub.Infrastructure.Data;

namespace Seeder
{
    class Program
    {
        public static void CreateRolesAndUsers(VideoclubDbContext db)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            if (!roleManager.RoleExists("Admin"))
            {
                var role = new IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);

                var user = new ApplicationUser();
                user.UserName = "admin@videoclub.com";
                user.Email = "admin@videoclub.com";
                var userPassword = "Admin123.";
                var userCreation = userManager.Create(user, userPassword);

                if (userCreation.Succeeded)
                {
                    userManager.AddToRole(user.Id, "Admin");
                    Console.WriteLine("Admin created!");
                }
            }

            if (!roleManager.RoleExists("Customer"))
            {
                var role = new IdentityRole();
                role.Name = "Customer";
                roleManager.Create(role);

                Console.WriteLine("Customer Role created!");
            }
        }

        public static void CreateMovies(VideoclubDbContext db)
        {
            Console.WriteLine("\n\nSeeding Movies...");

            var movies = new List<Movie>()
            {
                new Movie("Transformers", "Robot Cars", MovieGenre.Action, 2),
                new Movie("Jurassic Park", "Dinosaurs Reborn", MovieGenre.Adventure, 4),
                new Movie("Avatar", "Blue Aliens", MovieGenre.Comedy, 3),
                new Movie("John Wick", "Furius Guy", MovieGenre.Action, 2),
                new Movie("Insidious", "Scary movie", MovieGenre.Horror, 3)
            };

            foreach (var movie in movies)
            {
                db.Movies.Add(movie);
                db.SaveChanges();
                Console.WriteLine("Movie {0} added in DB!", movie.Title);
            }
        }

        static void Main(string[] args)
        {
            var context = new VideoclubDbContext();

            CreateRolesAndUsers(context);
            CreateMovies(context);

            Console.ReadKey();
        }
    }
}
